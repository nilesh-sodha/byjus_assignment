package com.example.streaming_tweets_dashboard_backend.model.nlp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SentimentAnalysisRequest {
    String text;
}
