package com.example.streaming_tweets_dashboard_backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nileshsodha
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TweetModel {
    String username;
    String tweet;
    String hashTag;
}
