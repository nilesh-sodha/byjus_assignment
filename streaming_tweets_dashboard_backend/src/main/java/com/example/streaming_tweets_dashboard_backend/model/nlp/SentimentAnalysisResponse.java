package com.example.streaming_tweets_dashboard_backend.model.nlp;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @author nileshsodha
 */
@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class SentimentAnalysisResponse {
    int veryPositive;
    int positive;
    int neutral;
    int negative;
    int veryNegative;
    int sentimentScore;
    String sentimentType;
    String text;
}
