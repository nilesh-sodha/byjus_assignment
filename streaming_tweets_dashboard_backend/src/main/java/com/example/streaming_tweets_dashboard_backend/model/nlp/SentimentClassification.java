package com.example.streaming_tweets_dashboard_backend.model.nlp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nileshsodha
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SentimentClassification {
    int veryPositive;
    int positive;
    int neutral;
    int negative;
    int veryNegative;
}
