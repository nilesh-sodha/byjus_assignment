package com.example.streaming_tweets_dashboard_backend.controller;

import com.example.streaming_tweets_dashboard_backend.properties.ApiPath;
import com.example.streaming_tweets_dashboard_backend.properties.KafkaTopicList;
import com.example.streaming_tweets_dashboard_backend.model.TweetModel;
import com.example.streaming_tweets_dashboard_backend.service.KafkaProducerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nileshsodha
 */

@Slf4j
@RestController
@Api(value = "Twitter Controller")
@RequestMapping(value = ApiPath.TWITTER)
public class TwitterController {

    @Autowired
    KafkaProducerService producerService;

    // To publish tweet into kafka
    @RequestMapping(value = ApiPath.PUBLISH_TWEET, method = {RequestMethod.GET})
    @ApiOperation(value = "To publish tweet on kafka")
    public boolean publishTweet(@RequestParam("username") String username,
        @RequestParam("tweet") String tweet, @RequestParam("hashTag") String hashTag) {
        TweetModel model = new TweetModel(username, tweet, hashTag);
        return producerService.publish(KafkaTopicList.TWEET, model);
    }
}
