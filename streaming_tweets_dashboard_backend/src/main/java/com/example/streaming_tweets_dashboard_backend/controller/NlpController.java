package com.example.streaming_tweets_dashboard_backend.controller;

import com.example.streaming_tweets_dashboard_backend.model.TweetModel;
import com.example.streaming_tweets_dashboard_backend.model.nlp.SentimentAnalysisRequest;
import com.example.streaming_tweets_dashboard_backend.model.nlp.SentimentAnalysisResponse;
import com.example.streaming_tweets_dashboard_backend.properties.ApiPath;
import com.example.streaming_tweets_dashboard_backend.properties.KafkaTopicList;
import com.example.streaming_tweets_dashboard_backend.service.SentimentAnalysisService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author nileshsodha
 */
@Slf4j
@RestController
@Api(value = "NLP Controller")
@RequestMapping(value = ApiPath.NLP)
public class NlpController {

    @Autowired
    SentimentAnalysisService sentimentAnalysisService;

    ObjectMapper mapper = new ObjectMapper();

    // To get sentimental analysis of text
    @CrossOrigin
    @RequestMapping(value = ApiPath.SENTIMENTAL_ANALYSIS, method = {RequestMethod.GET})
    @ApiOperation(value = "To get sentiment analysis data")
    public SentimentAnalysisResponse getSentimentData(@RequestParam String text) {
        SentimentAnalysisResponse response = null;
        try {
            response = sentimentAnalysisService.getSentimentData(text);
        } catch (Exception e) {
            log.error("Error while parse request for SENTIMENTAL_ANALYSIS due to {}", e);
        }
        return response;
    }
}
