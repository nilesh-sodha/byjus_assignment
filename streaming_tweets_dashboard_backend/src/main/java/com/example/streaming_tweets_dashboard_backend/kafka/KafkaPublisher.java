package com.example.streaming_tweets_dashboard_backend.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author nileshsodha
 */
@Slf4j
@Component
public class KafkaPublisher {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    /**
     * To produce data into kafka in given topic and data
     * @param topic - kafka topic name
     * @param object - data
     * @return
     */
    public boolean publish(String topic, Object object) {
        boolean success = false;
        try {
            this.kafkaTemplate.send(topic, object);
            log.warn("Successfully data has produce in kafka for TOPIC : '{}' and Object : {}",
                topic, object);
            success = true;
        } catch (Exception e) {
            log.error(
                "Error occurred while producing data into kafka for TOPIC : '{}' and Object : {}due to : ",
                topic, object, e);
        }
        return success;
    }
}
