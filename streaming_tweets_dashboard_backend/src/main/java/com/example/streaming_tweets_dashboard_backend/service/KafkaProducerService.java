package com.example.streaming_tweets_dashboard_backend.service;

/**
 * @author nileshsodha
 */
public interface KafkaProducerService {

    /**
     *
     * @param topic - To publish event on given topic
     * @param data - Data of topic
     * @return
     */
    boolean publish(String topic, Object data);
}
