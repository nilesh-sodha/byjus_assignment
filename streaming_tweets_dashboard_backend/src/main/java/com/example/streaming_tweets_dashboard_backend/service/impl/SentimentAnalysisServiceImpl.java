package com.example.streaming_tweets_dashboard_backend.service.impl;

import com.example.streaming_tweets_dashboard_backend.model.nlp.SentimentAnalysisResponse;
import com.example.streaming_tweets_dashboard_backend.model.nlp.SentimentResult;
import com.example.streaming_tweets_dashboard_backend.nlp.SentimentAnalyzer;
import com.example.streaming_tweets_dashboard_backend.service.SentimentAnalysisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author nileshsodha
 */
@Slf4j
@Service
public class SentimentAnalysisServiceImpl implements SentimentAnalysisService {

    @Override
    public SentimentAnalysisResponse getSentimentData(String text) {
        SentimentAnalysisResponse analysisResponse = null;
        try {
            log.warn("Start sentiment analysis text : '{}'", text);
            SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();
            sentimentAnalyzer.initialize();
            SentimentResult sentimentResult = sentimentAnalyzer.getSentimentResult(text);

            // Setting data into response
            analysisResponse = new SentimentAnalysisResponse();
            analysisResponse.setVeryPositive(sentimentResult.getSentimentClass().getVeryPositive());
            analysisResponse.setPositive(sentimentResult.getSentimentClass().getPositive());
            analysisResponse.setNeutral(sentimentResult.getSentimentClass().getNeutral());
            analysisResponse.setNegative(sentimentResult.getSentimentClass().getNegative());
            analysisResponse.setVeryNegative(sentimentResult.getSentimentClass().getVeryNegative());
            analysisResponse.setSentimentScore(sentimentResult.getSentimentScore());
            analysisResponse.setSentimentType(sentimentResult.getSentimentType());
            analysisResponse.setText(text);
            log.warn("Completed sentiment analysis text : '{}'", text);
        } catch (Exception e) {
            log.error("Error occurred while getting sentiment analysis on text : '{}' due to {}",
                text, e);
        }
        return analysisResponse;
    }
}
