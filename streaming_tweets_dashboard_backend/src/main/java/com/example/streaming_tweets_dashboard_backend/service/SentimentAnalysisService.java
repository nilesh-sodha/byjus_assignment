package com.example.streaming_tweets_dashboard_backend.service;

import com.example.streaming_tweets_dashboard_backend.model.nlp.SentimentAnalysisResponse;

/**
 * @author nileshsodha
 */
public interface SentimentAnalysisService {

    /**
     * To get sentiment data on given text
     * @param text - any text/sentence
     * @return
     */
    SentimentAnalysisResponse getSentimentData(String text);
}
