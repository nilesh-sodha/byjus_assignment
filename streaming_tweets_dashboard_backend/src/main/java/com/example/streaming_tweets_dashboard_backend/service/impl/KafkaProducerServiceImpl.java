package com.example.streaming_tweets_dashboard_backend.service.impl;

import com.example.streaming_tweets_dashboard_backend.kafka.KafkaPublisher;
import com.example.streaming_tweets_dashboard_backend.service.KafkaProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author nileshsodha
 */
@Slf4j
@Service
public class KafkaProducerServiceImpl implements KafkaProducerService {

    @Autowired
    KafkaPublisher producer;

    @Override
    public boolean publish(String topic, Object data) {
        return producer.publish(topic, data);
    }
}
