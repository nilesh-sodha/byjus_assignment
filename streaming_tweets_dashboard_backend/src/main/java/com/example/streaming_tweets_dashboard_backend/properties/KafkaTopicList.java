package com.example.streaming_tweets_dashboard_backend.properties;

/**
 * @author nileshsodha
 * All kafka topic name list
 */
public interface KafkaTopicList {
    String TWEET = "tweet";
}
