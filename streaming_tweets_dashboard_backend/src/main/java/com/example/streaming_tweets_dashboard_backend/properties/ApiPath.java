package com.example.streaming_tweets_dashboard_backend.properties;

/**
 * @author nileshsodha
 * All api path
 */
public interface ApiPath {
    String TWITTER = "/twitter";
    String NLP = "/nlp";
    String PUBLISH_TWEET = "/tweet/publish/";
    String SENTIMENTAL_ANALYSIS = "/sentimental-analysis/";
}
