var express = require('express'),
    app = express(),
    fs = require('fs'),
    kafka = require('kafka-node'),
    Consumer = kafka.Consumer,
    client = new kafka.KafkaClient(),
    consumer = new Consumer(
        client,
        [
            {topic: 'tweet', partition: 0}
        ],
        {
            autoCommit: true
        }
    );
var router = express.Router();

// Kafka consumer for given topic
consumer.on('message', onMessage);

// On message stream data to respective clients
function onMessage(message) {
    console.log("Consumed message : ", message.value);
    // var tweet = JSON.parse(message.value);
    updateSseClients(message.value.toString());
}

var hashTagClientId = 0;
var hashTagMap = new Map();
// Register client for data streaming on requested hashTag 
router.get('/hashtag/:hashTag', function (req, res) {
    console.log("Request came");
    var hashTagValue = req.params.hashTag;
    req.socket.setTimeout(Number.MAX_VALUE);
    res.writeHead(200, {
        'Content-Type': 'text/event-stream', // <- Important headers
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');
    (function (hashTagClientId) {
        var clientList = hashTagMap.get(hashTagValue);
        if (clientList == undefined) {
            clientList = new Map();
        }
        clientList.set(hashTagClientId, res);
        hashTagMap.set(hashTagValue, clientList);
        req.on("close", function () {
            var clientList = hashTagMap.get(hashTagValue);
            clientList.delete(hashTagClientId);
            hashTagMap.set(hashTagValue, clientList);
        }); // <- Remove this client when he disconnects

        req.on("end", function () {
            var clientList = hashTagMap.get(hashTagValue);
            clientList.delete(hashTagClientId);
            hashTagMap.set(hashTagValue, clientList);
        });
    })(++hashTagClientId)
});

var usernameClientId = 0;
var usernameMap = new Map();
// Register client for data streaming on requested username
router.get('/username/:username', function (req, res) {
    console.log("Request came");
    var usernameValue = req.params.username;
    req.socket.setTimeout(Number.MAX_VALUE);
    res.writeHead(200, {
        'Content-Type': 'text/event-stream', // <- Important headers
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');
    (function (usernameClientId) {
        var clientList = usernameMap.get(usernameValue);
        if (clientList == undefined) {
            clientList = new Map();
        }
        clientList.set(usernameClientId, res);
        usernameMap.set(usernameValue, clientList);
        req.on("close", function () {
            var clientList = usernameMap.get(usernameValue);
            clientList.delete(usernameClientId);
            usernameMap.set(usernameValue, clientList);
        });

        req.on("end", function () {
            var clientList = usernameMap.get(usernameValue);
            clientList.delete(usernameClientId);
            usernameMap.set(usernameValue, clientList);
        });
    })(++usernameClientId)
});

// To streaming tweet data
function updateSseClients(data) {
    var jsonData = JSON.parse(data);

    var hashTagClientList = hashTagMap.get(jsonData['hashTag']);
    if (hashTagClientList != undefined) {
        for (var entry of hashTagClientList.entries()) {
            entry[1].write("data: " + data + "\n\n");
        }
    } else {
        console.log("No client found for HashTag : " + jsonData['hashTag']);
    }

    var usernameClientList = usernameMap.get(jsonData['username']);
    if (usernameClientList != undefined) {
        for (var entry of usernameClientList.entries()) {
            entry[1].write("data: " + data + "\n\n");
        }
    } else {
        console.log("No client found for Username : " + jsonData['username']);
    }
}

module.exports = router;