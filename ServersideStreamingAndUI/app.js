var express = require('express'),
    app = express(),
    fs = require('fs'),
    stream = require('./stream');

app.get("/", function (request, response) {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });
    // fs.readFile('./index.html', null, function (error, data) {
    fs.readFile('./index.html', null, function (error, data) {
        if (error) {
            response.writeHead(404);
            respone.write('Whoops! File not found!');
        } else {
            response.write(data);
        }
        response.end();
    });
})

app.use('/stream', stream);
app.use('/public', express.static('static'));

var port = 1111;
app.listen(1111, function () {
    console.log("Application is running on ", port)
});
