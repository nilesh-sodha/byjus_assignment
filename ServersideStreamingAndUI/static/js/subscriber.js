//To prevent refresh page on submit form
$('form').submit(function (evt) {
    evt.preventDefault();
})
var selectedRadioButtonValue = "HashTag";
var tweets = "";

function changeText(name) {
    console.log("Come to change name");
    var btnValue = name;
    if (btnValue == 'HashTag') {
        document.getElementById("display_text").innerHTML = "Enter HashTag :";
        this.selectedRadioButtonValue = "HasTag";
    }
    if (btnValue == 'Username') {
        document.getElementById("display_text").innerHTML = "Enter Username :";
        this.selectedRadioButtonValue = "Username";
    }
}

var tabledata = [];
var tableId = 1;

function startStreaming() {
    document.getElementById("input_data").disabled = true;
    document.getElementById("stream_data").disabled = false;
    var enteredValue = document.getElementById("entered_value").value;
    console.log("Entered value is ", enteredValue);
    if (selectedRadioButtonValue == 'HashTag') {
        var eventData = new EventSource(`/stream/hashtag/${enteredValue}`);
        document.getElementById("word_cloud_text").innerHTML = "HashTag : " + enteredValue;
        eventData.onmessage = function (data) {
            var jsonData = JSON.parse(data.data);
            console.log("Username : " + jsonData.username);
            console.log("Tweet : " + jsonData.tweet);
            console.log("Hashtag : " + jsonData.hashTag);
            var data = {id: tableId++, name: jsonData.username, tweet: jsonData.tweet, hashtag: jsonData.hashTag};
            tabledata.unshift(data);
            updateTable();
            tweets = tweets + " " + jsonData.tweet;
        };
    }
    if (selectedRadioButtonValue == 'Username') {
        var eventData = new EventSource(`/stream/username/${enteredValue}`);
        document.getElementById("word_cloud_text").innerHTML = "Username : " + enteredValue;
        eventData.onmessage = function (data) {
            var jsonData = JSON.parse(data.data);
            console.log("Username : " + jsonData.username);
            console.log("Tweet : " + jsonData.tweet);
            console.log("Hashtag : " + jsonData.hashTag);
            var data = {id: tableId++, name: jsonData.username, tweet: jsonData.tweet, hashtag: jsonData.hashTag};
            tabledata.unshift(data);
            updateTable();
            tweets = tweets + " " + jsonData.tweet;
        };
    }
}

updateTable()
var text;

function updateTable() {
    var printIcon = function (cell, formatterParams) { //plain text value
        return '<button class="btn">Click here to see sentiment analysis</button>';
    };
    var table = new Tabulator("#tweet-table", {
        // set height of table to enable virtual DOM
        data: tabledata, //load initial data into table
        layout: "fitColumns", //fit columns to width of table (optional)
        columns: [ //Define Table Columns
            {title: "Username", field: "name", sorter: "string", width: 150, sortable: false},
            {title: "Tweet", field: "tweet", sorter: "string", align: "left", sortable: false},
            {title: "HashTag", field: "hashtag", sorter: "string", align: "center", width: 150, sortable: false},
            {
                title: "Sentimental Analysis",
                sortable: false,
                width: 200,
                formatter: printIcon,
                align: "center",
                cellClick: function (e, cell) {
                    document.getElementById("sentimental_analysis").style.display = 'flex';
                    var text = cell.getRow().getData().tweet;
                    var request = new XMLHttpRequest()
                    request.open('GET', 'http://localhost:1000/nlp/sentimental-analysis/?text=' + text, true)
                    request.onload = function () {
                        var jsonData = JSON.parse(this.response);
                        document.getElementById("sentimental_text").innerText = "Text : " + jsonData.text;
                        var data = {
                            id: 0, sentimentType: jsonData.sentimentType, sentimentScore: jsonData.sentimentScore,
                            veryPositive: jsonData.veryPositive, positive: jsonData.positive, neutral: jsonData.neutral,
                            negative: jsonData.negative, veryNegative: jsonData.veryNegative
                        }
                        var sentimentalTableData = [];
                        sentimentalTableData.push(data);
                        var table = new Tabulator('#sentimental_analysis_table', {
                            data: sentimentalTableData,
                            layout: "fitColumns",
                            columns: [
                                {title: "Sentiment Type", field: "sentimentType", sorter: "string", sortable: false},
                                {title: "Sentiment Score", field: "sentimentScore", sorter: "string", sortable: false},
                                {title: "Very Positive", field: "veryPositive", sorter: "string", sortable: false},
                                {title: "Positive", field: "positive", sorter: "string", sortable: false},
                                {title: "Neutral", field: "neutral", sorter: "string", sortable: false},
                                {title: "Negative", field: "negative", sorter: "string", sortable: false},
                                {title: "Very Negative", field: "veryNegative", sorter: "string", sortable: false},
                            ]
                        });

                    }
                    request.send();
                }
            }
        ]
    });
}

document.getElementById("stream_data").disabled = true;